require './parse/gradle'
require './parse/ds'
require 'json/add/ostruct'

if ARGV.size < 3
  puts 'Usage: ruby main.rb [path to gradle dependencies output] [path to dependency scanning report] [path to where new report should be written]'
  exit 1
end

gradle_dependencies_file = ARGV[0]
dependency_scanning_report_file = ARGV[1]
output_report_file = ARGV[2]

_, map = GradleGraphParser::parse(File.open(gradle_dependencies_file))
report = DependencyScanningReport::parse(File.open(dependency_scanning_report_file))
report['vulnerabilities'].each do |vuln|
  dep = vuln.dig('location', 'dependency')
  next if dep.nil?
  key = "#{dep.dig('package', 'name').gsub('/', ':')}:#{dep.dig('version')}"
  node = map.get(key)

  path = [node]
  loop do
    node = node.parent

    break if node.nil? || node.root?

    path << node
  end
  dep['iid'] = path.first.iid
  vuln['details']['introduced_by_package'] = {
      type: 'text',
      name: 'Introduced by Package',
      value: path[-1].val
    }
  vuln['details']['shortest_path'] = {
    type: 'list',
    name: 'Shortest Path',
    items: path[1..].map do |node|
      { type: 'text', value: node.val }
    end
  }
end
report['dependency_files'].each do |dependency_file|
  dependency_file['dependencies'].each do |dep|
    key = "#{dep.dig('package', 'name').gsub('/', ':')}:#{dep.dig('version')}"
    node = map.get(key)
    path = [node]
    loop do
      node = node.parent

      break if node.nil? || node.root?

      path << node
    end
    dep['iid'] = path.first.iid
    dep['dependency_path'] = path[1..].map { |n| { iid: n.iid } }
  end
end

File.write(output_report_file, JSON.dump(report))
