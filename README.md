## Description

GitLab Dependency Scanning does not currently support graph data for gradle projects. This project hosts a simple script to decorate GitLab dependency scanning reports with gradle graph data.

## How it works

The script combines `gradle dependencies` output with dependency data in the dependency scanning report to create dependency path and vulnerable component chain data.
`
## How to use

### 1. On the command line

* the output of `gradle dependencies` stored in a file (i.e. `./dependencies.txt`)
* a dependency scanning report for the same gradle project (i.e. `./gl-dependency-scanning-report.json`)
* where to write the output (i.e. `./new-report.json`)

```ruby
ruby main.rb ./dependencies.txt ./gl-dependency-scanning-report.json ./new-report.json
```

### 2. In a ci job

In order to decorate dependency scanning output, we can chain a job calling this script after dependency scanning completes:

```yaml
include:
  - template: Dependency-Scanning.gitlab-ci.yml

# override the DS analyzer which will be triggered for gradle projects
gemnasium-maven-dependency_scanning:
 artifacts:
   reports:
    dependency_scanning: non-existent # override so that no report is outputted by the DS job
   paths:
     - ./gl-dependency-scanning-report.json # expose DS artifact
     - ./dependencies.txt # expose output of gradlew dependencies
 after_script:
   - gradlew dependencies > dependencies.txt

report generation:
  variables:
    GRADLE_FILE: ./dependencies.txt
    INPUT_REPORT: ./gl-dependency-scanning-report.json
    OUTPUT_REPORT: /tmp/new-report.json
  image: 'ruby:3.2'
  script:
    - git -C /tmp/ clone https://gitlab.com/ifrenkel/gradle-graph.git
    - cd /tmp/gradle-graph
    - ruby main.rb $GRADLE_FILE $INPUT_REPORT $OUTPUT_REPORT
    - mv $OUTPUT_REPORT $CI_PROJECT_DIR
  artifacts:
    reports:
      dependency_scanning: new-report.json
  needs: ['gemnasium-maven-dependency_scanning']
```

## What it looks like

The generated report will have the following attributes added:
* `.vulnerabilities[].location.dependency.iid`
* `.vulnerabilities[].details.shortest_path`
* `.vulnerabilities[].details.introduced_by_package`
* `.dependency_files[].dependencies[].iid`
* `.dependency_files[].dependencies[].dependency_path`