require 'json'
class DependencyScanningReport
  def self.parse(io)
    JSON.load(io)
  end
end