class Node
  def initialize(val, parent, root=false)
    @val = val.strip
    @parent = parent
    @children = []
    @iid = nil
    @root = root
  end

  def parent
    @parent
  end

  def add_child(node)
    @children << node
  end

  def last_child
    @children.last
  end

  def children
    @children
  end

  def val
    @val
  end

  def to_s
    val
  end

  def iid=(iid)
    @iid = iid
  end

  def iid
    @iid
  end

  def root?
    @root == true
  end
end

class DepMap
  def initialize
    @map = {}
    @iid = 1
  end

  def add(node)
    return if @map[node.val]

    iid = next_iid
    node.iid = iid
    @map[node.val] = node
  end

  def get(name_version)
    @map[name_version]
  end

  def next_iid
    @iid+=1
  end
end

class GradleGraphParser
  def initialize(io)
    @io = io
    @map = DepMap.new
  end

  def self.parse(io)
    parser = new(io)
    [parser.parse, parser.map]
  end
  def parse
    configs = []
    root = nil
    last_line = nil
    inconfig = false
    @io.each_line do |line|
      if line.strip == ''
        inconfig = false
        root = nil
        next
      end

      if !inconfig && (line[0] == '+' || line[0] == '\\')
        inconfig=true
        root = Node.new("config: #{last_line}", nil, true)
        configs << root
      end

      add_line_to_root(line, root)
      last_line = line
    end
    configs
  end

  def add_line_to_root(line, parent)
    token = line[0..4]
    rest = line[5..]
    case token
    when '+--- ', '\\--- '
      node = Node.new(rest, parent)
      @map.add(node)
      parent.add_child(node)
    when '|    ', '     '
      add_line_to_root(rest, parent.last_child)
    else
      #puts " -->> else #{token}, #{rest}"
    end
  end

  def map
    @map
  end
end

class GraphPrinter
  def self.print(start_node)
    _print(start_node, 0)
  end
  def self._print(start_node, level)
    indent = '-' * level
    puts "#{indent} #{start_node}"
    start_node.children.each do |node|
      _print(node, level+1)
    end
  end
end